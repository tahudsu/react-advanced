import passport from "passport"
import config from "../config.js"
import { User } from "../models/user.js"
import { Strategy, ExtractJwt } from 'passport-jwt';

import LocalStrategy from 'passport-local';
// create local strategy 
const localOptions = {usernameField: 'email'};
const localLogin = new LocalStrategy(localOptions,
    function(email, password, done) {
        // verify this username and password, call done with the user
        // if its the correct username and pass
        // otherwise call done with false
        User.findOne({ email: email }, function(err, user) {
            if (err) { return done(err)}

            if (!user) {
                return done(null, false)
            }

            // compare passwords - is password queal to user.password ?
            user.comparePassword(password, function(err, isMatch) {
                if (err) return done(err);
                if (!isMatch) return done(null, done);

                return done(null, user);
            })
        })
});
// setup options for jwt strategy
const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config.secret
}

// create jwt strategy
const jwtLogin = new Strategy(jwtOptions, function(payload, done) {
    // see if the user ID in the payload exists in our database
    // if it does call, done with that other
    // otherwise, call done without a user object
    User.findById(payload.sub, function(err, user) {
        if (err) return done(err, false);

        if (user) {
            done(null, user)
        } else {
            done(null, false);
        }
    })
});

// tell passport to use this strategy
passport.use(jwtLogin);
passport.use(localLogin);
export default passport;