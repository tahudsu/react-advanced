import Mongoose from "mongoose";
import bcrypt from 'bcrypt-nodejs';

const Schema = Mongoose.Schema;

// define our model
const userSchema = new Schema({
    email: { type: String, unique: true, lowercase: true },
    password: { type: String }
})

// OnSave hook, encrypt password
// before saving a model run this method
userSchema.pre('save', function(next) {
    const user = this;
    
    bcrypt.genSalt(10, function(err, salt) {
        if (err) {
            return next(err);
        }

        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err) {
                return next(err);
            }

            user.password = hash;
            next();
        })
    })
})

userSchema.methods.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) {
            return callback(err);
        }

        callback(null, isMatch);
    })
}
// create the model class
export const User = Mongoose.model('user', userSchema);
// export the model