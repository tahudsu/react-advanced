
import { signup, singin } from './controllers/authentication.js';
import passport from 'passport';
import  passportService from './services/passport.js';
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false})

export const router = (app) => {
    app.get('/', requireAuth, (req, res, next) => {
        res.send({ hi: "there"});
    }) 
    app.post('/signin',requireSignin, singin)
    app.post('/signup', signup);  
}