// import { User } from '../models/user';
import jwt from 'jwt-simple';
import { User } from '../models/user.js';
import config from '../config.js';

function tokenForUser(user) {
    const timestamp = new Date().getTime();
    return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

export const singin = (req, res, next) => {
    // user has already had their email and pass auth`d
    // we just need to give them token
    res.send({ token: tokenForUser(req.user)});
}

export const signup = (req, res, next) => {
    // res.send({ success: true });
    // see if a user with the given email exists
    console.log(req.body);
    const email = req.body.email;
    const password = req.body.password;
    // if a user dos exist, return an error
    User.findOne({ email: email, password: password }, (err, existingUser) => {
        if (err) {
            return next(err);
        }
        if (existingUser) {
            return res.status(422).send({ error: 'Email is in use'});
        }

    // if a user with email does not exit, create and save user record
        const user = new User({
            email,
            password
        })
        user.save((err) => {
            if (err) {
                return next(err);
            }

            res.json({ token: tokenForUser(user) });
        });
    // Respond to reuqest indicating the user was created

    })

}