// main starting point of the application
import bodyParser from 'body-parser';
import express from 'express';
import http from 'http';
import mongoose from 'mongoose';

import morgan from 'morgan';
import { router } from './router.js';
import cors from 'cors';

// DB setup
mongoose.connect('mongodb://localhost:auth/auth',  { useNewUrlParser: true});

// app setup
const app = express();
app.use(morgan('combined'));
app.use(cors());
// change bodyparser
app.use(bodyParser.json({ type: '*/*'}));
router(app);

// server setup
const port = process.env.PORT | 3090;

const server = http.createServer(app);
server.listen(port);

console.log('Server listening on :', port);