const { SAVE_COMMENT } = require("actions/types");
const { default: comments } = require("reducers/comments");

it('handles actions of type SAVE_COMMENT', () => {
    const action = {
        type: SAVE_COMMENT,
        payload: 'New Comment'
    }

    const newState = comments([], action);
    expect(newState).toEqual(['New Comment']);

});

it('handles action with unknown type', () => {
    const newState = comments([], { type: 'gnkjasgan'});

    expect(newState).toEqual([]);
});