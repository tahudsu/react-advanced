import React from 'react';
import reduxPromise from 'redux-promise';
import { composeWithDevTools } from 'redux-devtools-extension';
import async from 'middlewares/async';
import stateValidator from 'middlewares/stateValidator'
const { Provider } = require("react-redux");
const { default: reducers } = require("reducers");
const { createStore, applyMiddleware } = require("redux");

export const Root = ({ children, initialState = {}}) => {
    const composedEnhancer = composeWithDevTools(
        applyMiddleware(
            // reduxPromise,
            async,
            stateValidator
            ),
    )
    
    const store = createStore(
        reducers,
        initialState, 
        composedEnhancer
       //  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
        )

    return (
        <Provider store={ store }>
            { children }
          </Provider>
    )
}
