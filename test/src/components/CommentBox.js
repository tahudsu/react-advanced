import React, { useEffect, useState } from 'react'
import * as actions from 'actions';
import { useDispatch, useSelector } from 'react-redux';
import { requireAuth } from 'components/requireAuth';

const _CommentBox = (props) => {
    const initialState = {
        message: ''
    }
    const [state, setState] = useState(initialState);
    
    const dispatch = useDispatch();

    const handleChange = (e) => {
        setState({ ...state, message: e.target.value })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(state);
        dispatch(actions.saveComment(state.message));
        setState(initialState);
    }
    return (
        <div>
            <h4>Add a comment</h4>
            <form onSubmit={handleSubmit}>
                <textarea value={state.message} onChange={handleChange} />
                <div>
                    <button type="submit">Submit Comment</button>
                
                </div>
            </form>
            <button className="fetch-comments" onClick={ () => dispatch(actions.fetchComments()) }>Fetch Comments</button>
            <p style={ { fontSize: "40px"} }>{ state.message }</p>
        </div>
    )
}
export const CommentBox = requireAuth(_CommentBox);

