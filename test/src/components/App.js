import { changeAuth } from 'actions'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Route } from 'react-router'
import { Link } from 'react-router-dom'
import { CommentBox } from './CommentBox'
import { CommentList } from './CommentList'

export const App = () => {
    const { auth } = useSelector(state => state);
    const dispatch = useDispatch();

    const authHandler = (e) => {
        console.log(e);
        const value = e.target.className === 'in' ? true : false;
        dispatch(changeAuth(value));
    }

    const renderButton = () => {
        if (auth) {
            return (
                <button className="out"
                onClick={
                    authHandler
                }>Sign Out</button>
            )
        }

        return (
            <button  className="in" onClick={
                authHandler
            }>Sign In</button>
        )
    }

    const renderHeader = () => {
        return (
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/post">Post a comment</Link>
                </li>
                <li>
                    { renderButton() }
                </li>
            </ul>
        )
    }

    return (
        <div>
            { renderHeader() }
            <Route path="/post" component={ CommentBox } />
            <Route path="/" exact component={ CommentList } />
        </div>
    )
}
