const { CommentBox } = require("components/CommentBox");
const { mount } = require("enzyme");
const { Root } = require("Root");

let wrapper;
beforeEach(() => {
    wrapper = mount(
        <Root>
            <CommentBox />

            </Root>
    );
})

afterEach(() => {
    wrapper.unmount();
})

describe('CommentBox tests', () => {
    it('CommentBox has a textarea and two buttons', () => {
        expect(wrapper.find('textarea').length).toEqual(1);
        expect(wrapper.find('button').length).toEqual(2);
    
    })
    
    it('CommentBox has a textarea that users can type in', () => {
        // add value to textarea
        wrapper.find('textarea').simulate('change', { target: { value: 'new comment'}});
        // force to rerender component state
        wrapper.update();
        // search value in textarea in rerendered component
        expect(wrapper.find('textarea').prop('value')).toBe('new comment');
    })
    
    it('CommentBox has a textarea that users can type in then submit and reset textarea', () => {
        // add value to textarea
        wrapper.find('textarea').simulate('change', { target: { value: 'new comment'}});
        // force to rerender component state
        wrapper.update();
        // search value in textarea in rerendered component
        expect(wrapper.find('textarea').prop('value')).toBe('new comment');
        wrapper.find('form').simulate('submit');
        wrapper.update();
        expect(wrapper.find('textarea').prop('value')).toEqual('');
    })
    
})
