const { CommentList } = require("components/CommentList");
const { mount } = require("enzyme");
const { Root } = require("Root");

describe('CommentList tests', () => {
    let wrapped;
    beforeEach(() => {
        const initialState = {
            comments: ['Comment 1', 'Comment 2']
        };
        
        wrapped = mount(
            <Root initialState={initialState}>
                <CommentList />
            </Root>
        )
    })

    it('creates one Li per comment', () => {
        expect(wrapped.find('li').length).toEqual(2);
    })

    it('comments are visible', () => {
        console.log(wrapped.render().text());
        expect(wrapped.render().text()).toContain('Comment 1')
        expect(wrapped.render().text()).toContain('Comment 2')

    })
})
