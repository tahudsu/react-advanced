const { saveComment } = require("actions");
const { SAVE_COMMENT } = require("actions/types");
const { act } = require("react-dom/test-utils");

describe('saveComment tests', () => {
    it('has the correct type', () => {
        const action = saveComment();

        expect(action.type).toEqual(SAVE_COMMENT);
    })

    it('has correct payload', () => {
        const action = saveComment('New Comment');
        expect(action.payload).toEqual('New Comment')
    })
})
