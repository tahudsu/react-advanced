import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { reduxForm, Field } from 'redux-form'
import { signup } from '../../actions';
const _Signup = (props) => {
    const dispatch = useDispatch();
    const auth = useSelector(state => state.auth);
    console.log(auth);
    const { handleSubmit } = props;
    const onSubmit = (formProps) => {
        console.log(formProps);
        dispatch(signup(formProps, () => {
           props.history.push('/feature'); 
        }));
    }
    return (
        <form onSubmit={ handleSubmit(onSubmit) }>
            <fieldset>
                <label>Email</label>
                <Field
                    name='email'
                    type="text"
                    component="input"
                    autoComplete="none"
                />
            </fieldset>
            <fieldset>
                <label>Password</label>
                <Field
                    name="password"
                    type="password"
                    component="input" 
                    autoComplete="none"
                    />
            </fieldset>
            <div>
                { auth.errorMessage }
            </div>
            <button>Sign up!</button>
        </form>
    )
}

export const Signup = reduxForm({ form: 'signup' })(_Signup);


