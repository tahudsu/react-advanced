import React from 'react'
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import './HeaderStyle.css'
export const Header = () => {
    const { auth } = useSelector(state => state)
    const renderLinks = () => {
        if (auth.authenticated) {
            return (
                <div>
                    <Link to="/signout">
                        Sign Out
                    </Link>
                    <Link to="/feature">
                        Feature
                    </Link>
                </div>
            )

        } else {
            return (
                <div>
                    <Link to="/signup">Sign Up</Link>
                    <Link to="/signin">Sign In</Link>
                </div>
            )
        }
    }
    return (
        <div className="header">
            <Link to="/">Home</Link>

            { renderLinks() }



        </div>
    )
}
