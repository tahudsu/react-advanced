import React, { Component, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';

// HOC High order component
export const requireAuth = (ChildComponent) => props => {
    const { auth } = useSelector(state => state)
    
    useEffect(() => {
        shouldNavigateAway();
    }, [auth.authenticated])
    
    const shouldNavigateAway = () => {
        if (!auth.authenticated) {
            console.log('I need to levae!!!');
            // redirect
            props.history.push('/');
        }
    }
    return <ChildComponent { ...props } />;

}