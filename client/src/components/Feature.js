import React from 'react'
import { requireAuth } from './requireAuth'

const _Feature = () => {
    return (
        <div>
            this is the feature!
        </div>
    )
}

export const Feature = requireAuth(_Feature);
